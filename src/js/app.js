import "@polaris/dashboard-sdk/src/css/styles.scss";
import {
  getResult,
  initGrid,
  LineChartWidget,
  BarChartWidget,
  AreaChartWidget,
  PieChartWidget,
  StackedBarChartWidget,
  SimpleGroupedBarChartWidget
} from "@polaris/dashboard-sdk";
import { CourseRatingChart } from "./custom-charts/courseRatingChart";

/**
 * JWT Token - hardcoded for demotrastion
 * This jwt token is generated in the backend.
 */
let token =
  "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJjb250ZXh0X2lkIjoidXNlcjFAcG9sYXJpcy5jb20iLCJleHAiOjE2Nzg5NzA4NTIsImVuZ2luZXMiOlsxLDQsNSwyLDVdLCJlbmdpbmVzX3dpdGhfY29udGV4dCI6WzYsN119.WxxMD5BWWHw5o_X1bbtfNP9ExFfbZjr-qam5g7xaxCq-ri0cu0USTD_8ux6jKqJLupVVUtysHxxJc9w03a6IY7Sn63tMLnP1lswm8SNrGr21RFg3n95Nf4vNGzeDNuT2_gdqUX65_wnf504K-f770JP7WcdDuJyZTLbgs0sFBUiM8VtKvRpHzVyEGszvIO4rnJiME5IL7yzUR92kcz1zqcL9If53a9vakf2y0BQRZTzUsdPokDDHMGyzCwJZq0tHG1d-06ZNl4XvkCD_96mdsj79oC26QrDN0zQBeOqHDmTiPUnLJFQkPwBhipsnnwKJ_zJkKMuEqlzlZm01wEhwdpj1dmxthiuC7EY8mvpmSDD73EStmjf7BOvwu9cYoDOeJXeSg2pdiqw9E1XfsGj7Va7UY1km8N1p5ZZzzzsrOVuX6hDZTQ7n1pjI2dnLO0I0QFpd30EOWtqOO58fQyD564UDWkX-avGahFKm3mRJ8H42PdEtslkSP9AO7ztFWa1Du8NcchyTW2Iomh-ztJGk4kCTR37eyumP9yDdzfQLsVPEbUtgZrJXfPKwA57eeD5BDNey15qLdA_HGF8FueMDZ-Y1f6C039WjoWQETy_TA17-VSvITy437sCq3sGDa9-ErNwc-K7E3dfiGHFxch_2zMI3Gm-vmlMoWO0IMAXrmBA";

// Rights Engine Endpoint
let url = "http://localhost:8003/api/v1/provider/result";

const subGrid = [
  { x: 0, y: 0, w: 4, h: 4, widgetId: "second-widget" },
  { x: 4, y: 0, w: 4, h: 4, widgetId: "second-widget" },
];

/**
 * Setup initial widget position and sizes
 */
const widgets_confgg = [  
  {
    x: 4,
    y: 0,
    w: 4,
    h: 8,
    widgetId: "first-widget",
  },
  {
    x: 4,
    y: 0,
    w: 4,
    h: 8,
    widgetId: "second-widget",
  },
  {
    x: 4,
    y: 0,
    w: 4,
    h: 4,
    widgetId: "fourth-widget",
  },
  {
    x: 8,
    y: 4,
    w: 4,
    h: 8,
    widgetId: "sixth-widget",
  },
  {
    x: 4,
    y: 8,
    w: 4,
    h: 8,
    widgetId: "seventh-widget",
  },
  {
    x: 8,
    y: 8,
    w: 4,
    h: 8,
    widgetId: "eigth-widget",
  },
  {
    x: 8,
    y: 12,
    w: 4,
    h: 8,
    widgetId: "ninth-widget",
  },
  {
    x: 0,
    y: 12,
    w: 4,
    h: 8,
    widgetId: "tenth-widget",
  },
  {
    x: 0,
    y: 12,
    w: 4,
    h: 8,
    widgetId: "eleventh-widget",
  },
  { x: 0, y: 0, w: 8, h: 4, subGrid: { children: subGrid, id: "sub1_grid", class: "subgrid" } },

];

/**
 * Handle description callback for widgets.
 * Shows modal with analytics engine description.
 * @param {*} desc
 */
const onShowDesc = (desc) => {
  const modalContent = document.getElementById("modal-content-body");
  modalContent.innerText = desc?.de ?? "-";
  const modal = new bootstrap.Modal(document.getElementById("myModal"), {});
  modal.show();
};

/**
 * Opens bootstrap error modal.
 * @param {*} message
 */
const showErrorModal = (message) => {
  const modalContent = document.getElementById("error-modal-content-body");
  modalContent.innerText = message;
  const modal = new bootstrap.Modal(document.getElementById("myErrorModal"), {});
  modal.show();
};


const changeMeToDataFromTheRightsEngine = {
  "2023-04-28": {
      "accessed": 98,
      "started": 7,
      "graded": 7,
      "submitted": 7,
      "answered": 38,
      "leftUnanswered": 4
  },
  "2023-04-29": {
      "accessed": 2
  },
  "2023-04-30": {
      "accessed": 1
  },
  "2023-05-01": {
      "accessed": 1
  },
  "2023-05-02": {
      "accessed": 45,
      "started": 4,
      "submitted": 4,
      "answered": 16,
      "leftUnanswered": 8,
      "graded": 2
  }
}

/**
 *  Built widgets from results data.
 * @param {*} data
 * @returns
 */
const buildWidgets = (data) => {
  const widgets = {
    "first-widget": new SimpleGroupedBarChartWidget(
      "Grouped Statements H5P",
      "",
      changeMeToDataFromTheRightsEngine
    ),
    "second-widget": new BarChartWidget(
      "Statements H5P",
      data["count_h5p_statements"]?.description,
      data["count_h5p_statements"]?.latest_result,
      {
        xAxisLabel: "Datum",
        yAxisLabel: "#Statements",
        transform: (d) => ({
          column1: new Date(d.column1 * 1000),
          column2: d.column2,
        }),
        onShowDesc,
      }
    ),
    "fourth-widget": new PieChartWidget(
      "Persönliche Statement Verteilung",
      data["h5p_statements_distribution"]?.description,
      data["h5p_statements_distribution"]?.latest_result,
      {
        showLegend: true,
        xAxisLabel: "Jahr",
        yAxisLabel: "#Akitivitäten",
        onShowDesc,
      }
    ),
    "sixth-widget": new LineChartWidget(
      "Semesterabschluss",
      data["collect_h5p_count_statements"]?.description,
      data["collect_h5p_count_statements"]?.latest_result,
      {
        xAxisLabel: "Monat",
        yAxisLabel: "Index",
        transform: (d) => ({
          column1: new Date(d.column1 * 1000),
          column2: d.column2,
        }),
        onShowDesc,
      }
    ),
    "seventh-widget": new CourseRatingChart(
      "Bewertungen für Kurse",
      data["random_course_rating"]?.description,
      data["random_course_rating"]?.latest_result,
      {
        xAxisLabel: "Note",
        yAxisLabel: "Kurs",
        onShowDesc,
      }
    ),
    "eigth-widget": new AreaChartWidget(
      "Statements H5P",
      data["collect_h5p_count_statements"]?.description,
      data["collect_h5p_count_statements"]?.latest_result,
      {
        xAxisLabel: "Monat",
        yAxisLabel: "Index",
        transform: (d) => ({
          column1: new Date(d.column1 * 1000),
          column2: d.column2,
        }),
        onShowDesc,
      }
    ),
    "ninth-widget": new BarChartWidget(
      "Statements Moodle",
      data["count_moodle_statements"]?.description,
      data["count_moodle_statements"]?.latest_result,
      {
        xAxisLabel: "Datum",
        yAxisLabel: "#Statements",
        transform: (d) => ({
          column1: new Date(d.column1 * 1000),
          column2: d.column2,
        }),
        onShowDesc,
      }
    ),
    "tenth-widget": new StackedBarChartWidget(
      "Statements",
      data["collect_counts_all_providers"]?.description,
      data["collect_counts_all_providers"]?.latest_result,
      ["overall"],
      {
        showLegend: true,
        xAxisLabel: "Provider",
        yAxisLabel: "#Statements",
        onShowDesc,
      }
    ),
    "eleventh-widget": new BarChartWidget(
      "Personal H5P xAPI Statements",
      data["h5p_count_user_statements"]?.description,
      data["h5p_count_user_statements"]?.latest_result,
      {
        xAxisLabel: "Datum",
        yAxisLabel: "#Statements",
        transform: (d) => ({
          column1: new Date(d.column1 * 1000),
          column2: d.column2,
        }),
        onShowDesc,
      }
    ),
  };

  return widgets;
};

const setupGrid = (data) => {
  //  Create widgets with data from analytics engine results

  const widgets = buildWidgets(data);

  // Initialize grid with widgets at configured positions
  grid = initGrid(widgets, widgets_confgg);

  // Handle toggle button click
  const toggleBtn = document.getElementById("toggle-sidebar-btn");
  toggleBtn.onclick = grid.toggleSidebar;

  // Handle save button click
  const saveBtn = document.getElementById("save-btn");
  saveBtn.onclick = () => {
    // Get current grid configuration -> we might want to store this configuration
    const oldGrid = grid.save();

    // Reload grid with saved grid configuration (just for demonstration)
    grid.grid.removeAll();
    setTimeout(() => {
      grid.load(oldGrid);
    }, 2000);
  };
};

let grid = null;

const onInit = () => {
  // Set default settings value in UI
  document.getElementById("endpointInput").value = url;
  document.getElementById("tokenInput").value = token;

  const saveSettingsBtn = document.getElementById("saveSettingsBtn");
  saveSettingsBtn.addEventListener("click", handleSaveSettingsClick);

  /**
   * Get analytics engine results and render widgets
   */
  // uncomment to show diagrams without data
  //setupGrid({})
  getResult(token, url)
    .then(setupGrid)
    .catch((err) => {
      showErrorModal(err.message);
    });
};

const handleSaveSettingsClick = () => {
  url = document.getElementById("endpointInput").value;
  token = document.getElementById("tokenInput").value;

  getResult(token, url)
    .then((data) => {
      if (grid) {
        const newWidgets = buildWidgets(data);
        grid.refreshWidgets(newWidgets);
      } else setupGrid(data);
    })
    .catch((err) => {
      showErrorModal(err.message);
    });
};

onInit();
